import React, { Component } from 'react';

class TodoForm extends Component {
    constructor(props){
        super(props);
        this.state = {inputValue: ''};
        this.handelChange = this.handelChange.bind(this);
        this.handelSubmit = this.handelSubmit.bind(this);
    }
    handelChange(e){
        this.setState({inputValue: e.target.value})
    }
    handelSubmit(){
        this.props.addTodo(this.state.inputValue)
        
    }
    render() { 
        return ( 
            <div> 
                <input type="text" 
                value={this.state.inputValue}
                onChange={this.handelChange}/>
                <button onClick={this.handelSubmit}> Add Todo</button>
            </div>
         );
    }
}
 
export default TodoForm;