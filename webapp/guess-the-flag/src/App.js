import React, { Component } from 'react';

import './App.css';
import Game from './Game'

class App extends Component {
  render() { 
 

   
    return (
      <div className="App">
        <header
          className="titel">
          <h1> Guess The Flag</h1>
        </header>
        <Game />
      </div>
    );
  }
}

export default App;
