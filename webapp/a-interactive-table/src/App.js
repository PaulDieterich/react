//https://reactjs.org/docs/thinking-in-react.html

import React, { Component } from 'react';

import './App.css';

class ProductCategroryRow extends Component{
render(){
  const category = this.props.category;
  return( <tr>
    <th colSpan="2"> {category}</th>
  </tr> )
}
}
class ProductRow extends Component{
  render(){
    const product = this.props.product;
    let name = product.stocked ? product.name : <span style={{color: "red"}} > {product.name}</span>
    return(
      <tr>
        <th>{name}</th> <th> {product.price}</th>
      </tr>
    )  
  }
}
class ProductTable extends Component{
  render(){
    const filterText = this.props.filterText;
    const inStockOnly = this.props.inStockOnly;
    const rows = [];
    let lastCategory = null;
    

    this.props.products.forEach((p) =>{
      if(p.name.indexOf(filterText) === -1){
        return;
      }
      if(inStockOnly && !p.stocked){
        return;
      }
      if(p.category !== lastCategory){
        rows.push(<ProductCategroryRow category={p.category} key={p.category}/>)
      }
      rows.push(<ProductRow product={p} key={p.name}></ProductRow>)
      lastCategory = p.category;
    })
    
    return(<table>
            <tead>
              <tr>
                <th> Name</th>
                <th> Preis</th>
              </tr>
            </tead>
            <tbody> {rows}</tbody>
          </table>)
  }
}
class SearchBar extends Component{
  constructor(props){
    super(props);
    this.handelFilterTextChange =  this.handelFilterTextChange.bind(this);
    this.handelInStockChange = this.handelInStockChange.bind(this);
  }
  handelFilterTextChange(e){
    this.props.onFilterTextChange(e.target.value);
  }
  handelInStockChange(e){
    this.props.onInStockChange(e.target.checked);
  }
  
  render(){
    const filterText = this.props.filterText;
    const inStockOnly = this.props.inStockOnly;
    return(
      <from>
        <input 
          type="text" 
          placeholder="Search...." 
          value={filterText}
          onChange={this.handelFilterTextChange}
          
          />
        <p>
          <input 
            type="checkbox" 
            checked={inStockOnly}
            onChange={this.handelInStockChange}
            />
           {' '} Only show products in stock
        </p>
      </from>
     
      );
  }
}

class App extends Component {
  constructor(props){
    super(props);
    this.state ={
      filterText: '',
      inStockOnly: false
    }
    this.handelFilterTextChange = this.handelFilterTextChange.bind(this);
    this.handelInStockChange = this.handelInStockChange.bind(this);
  }
  handelFilterTextChange(filterText){
    this.setState({filterText: filterText})
  }
  handelInStockChange(inStockOnly){
    this.setState({inStockOnly: inStockOnly})
  }
  render() {
    return (
      <div className="App">
        <SearchBar 
          filterText={this.state.filterText}
          inStockOnly={this.state.inStockOnly}
          onFilterTextChange={this.handelFilterTextChange}
          onInStockChange={this.handelInStockChange}/>
        <ProductTable 
        products={PRODUCTS}
        filterText={this.state.filterText}
        inStockOnly={this.state.inStockOnly}/>
      </div>
    );
  }
}
const PRODUCTS = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

export default App;
