import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      todos:[],
      inputTodo: ''
    }
    this.handelClick =this.handelClick.bind(this);
  }



handelClick(e){
  if(this.state.inputTodo === ''){
    e.preventDefault();
    alert("Es gab keine eingabe")
  }else{
    e.preventDefault();
    const todos  = [...this.state.todos, this.state.inputTodo]
    this.setState({todos , inputTodo:''})
  }

}
  render() { 
    let {inputTodo} = this.state;
    let todoList = this.state.todos.map((data, index) =>(
      <li key={index}> {data} </li>
    ))
    return (
      <div className="App">
        <h1>  Simple Todo App</h1>
        <form onSubmit={this.handelClick }> 
          <input
              type="text" 
              name="inputTodo"
              value={inputTodo}
              onChange={(e) => this.setState({[e.target.name]: e.target.value })}
          >
          </input>
          <button type="submit"> Click</button>
        </form>
        <div className="todo-content">
          <ol>
            {todoList}
          </ol>
        </div>
        
      </div>
    );
  }
}

export default App;
