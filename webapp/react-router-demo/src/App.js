import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import './App.css';
import SwitchDemo from './router';

class App extends Component {
  render() {
      return (
      <div className="App">
          <NavLink exact  to="/">
              HOME
          </NavLink>
          <NavLink exact  to="/about">
            ABOUT
          </NavLink>
     
        <div >
          <SwitchDemo/>
        </div>
      </div>
    );
  }
}

export default App;
