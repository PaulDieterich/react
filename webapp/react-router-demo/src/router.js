import React from 'react';
import {Switch,  Route} from 'react-router-dom';

const Homepage = () => (
  <h1>HOMEPAGE</h1>
);
const About = () => (
  <h1>ABOUT</h1>
);
const NotFound = () => (
    <h1>NotFound</h1>
  );
  

 const SwitchDemo = () => (
      <Switch>
            <Route exact path="/about" component={About}/>
            <Route exact path="/" component={Homepage}/>
            <Route component={NotFound}/>
      </Switch>
 );
export default SwitchDemo;