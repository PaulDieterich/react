//https://reactjs.org/docs/thinking-in-react.html

import React, { Component } from 'react';

import './App.css';

class ProductCategroryRow extends Component{
render(){
  const category = this.props.category;
  return( <tr>
    <th colSpan="2"> {category}</th>
  </tr> )
}
}
class ProductRow extends Component{
  render(){
    const product = this.props.product;
    let name = product.stocked ? product.name : <span style={{color: "red"}} > {product.name}</span>
    return(
      <tr>
        <th>{name}</th> <th> {product.price}</th>
      </tr>
    )  
  }
}
class ProductTable extends Component{
  render(){
    const rows = [];
    let lastCategory = null;
    

    this.props.products.forEach((p) =>{
      if(p.category !== lastCategory){
        rows.push(<ProductCategroryRow category={p.category} key={p.category}/>)
      }
      rows.push(<ProductRow product={p} key={p.name}></ProductRow>)
      lastCategory = p.category;
    })
    
    return(<table>
            <tead>
              <tr>
                <th> Name</th>
                <th> Preis</th>
              </tr>
            </tead>
            <tbody> {rows}</tbody>
          </table>)
  }
}
class SearchBar extends Component{
  render(){
    return(
      <from>
        <input type="text" placeholder="Search...."/>
        <p>
          <input type="checkbox"/>
           {' '} Only show products in stock
        </p>
      </from>
     
      );
  }
}
class App extends Component {
  render() {
    return (
      <div className="App">
        <SearchBar/>
        <ProductTable products={PRODUCTS}></ProductTable>
      </div>
    );
  }
}
const PRODUCTS = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

export default App;
