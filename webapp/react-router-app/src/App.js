import React, { Component } from 'react';
import NavBar from './components/NavBar/navbar';
import Router from './router' 
import './App.css';



class App extends Component {
  render() {
    return (
      <div className="App">

        <NavBar className="navbar"/>
        <div className="main">
          <Router/>
        </div>
         
      </div>
      
    );
  }
}

export default App;
