import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './components/Home/home'; 
import NotFound from './components/NotFound/notFound'; 
import About from './components/About/about';

const home = () =>( <Home></Home>);
const about = () =>( <About></About>);
const notFound = () => (<NotFound></NotFound>);

const Router = () => (
    <Switch>
          <Route exact path="/about" component={about}/>
          <Route exact path="/" component={home}/>
          <Route component={notFound}/>
    </Switch>
);
export default Router; 