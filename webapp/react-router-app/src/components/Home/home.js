import React, { Component } from 'react';
import {Carousel, Grid, Row, Col} from 'react-bootstrap' 
import Main from './main';
import LeftSidebar from './leftSidebar'


class Home extends Component {
    state = {  }
    render() { 
        return (
            <div className="home">
              <Carousel>
                <Carousel.Item>
                    <Carousel.Caption>
                    <img width={900} height={500} alt="900x500" src=""/>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img width={900} height={500} alt="900x500" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQviJZTdj58TsfltF6k4XDEgEyYp8PG9ABmkjF90UzZvR6vSOPeuA" />
                    <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img width={900} height={500} alt="900x500" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRxUDZxoH7TNN7fJ428ZvQxgudEtCZ7_pAw2pciT75KPa9U-Dg" />
                    <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel> 
            <Grid>
                <Row className="show-grid">
                    <Col md={8}>
                         <Main></Main>
                    </Col>
                    <Col md={4}>
                         <LeftSidebar dokumente={Dokumente}></LeftSidebar>
                    </Col>
                </Row>
                
            </Grid>

            </div>
         );
    }
}


export default Home;


const Dokumente = [
    {name:"name1", doc: "...1"},
    {name:"name2", doc: "...2"},
    {name:"name3", doc: "...3"},
    {name:"name4", doc: "...4"},
]