import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import { Nav,Navbar, NavItem } from 'react-bootstrap';
import './navbar.css';

class NavBar extends Component {
    state = {  }
    render() { 
        return ( 
           <Navbar  collapseOnSelect>
               <Navbar.Header>
                   <Navbar.Brand><p> Head</p> </Navbar.Brand>
               </Navbar.Header>
               <Navbar.Toggle />
               <Navbar.Collapse>
               <Nav>
                   <NavItem ><NavLink className="link"  to="/"> Home </NavLink></NavItem>
                   <NavItem ><NavLink className="link" to="/about"> about </NavLink></NavItem>
               </Nav>
               </Navbar.Collapse>
           </Navbar>
           
         );
    }
}
 
export default NavBar;

