import React, { Component } from 'react';


import Navbar from './Navbar';
import './RecipeApp.css';
import RecipeList from './RecipeList';
import RecipeInput from './RecipeInput';



class RecipeApp extends Component {
  constructor(props){
    super (props);
    this.state ={
      recipes: [
        {
          id:0,
          title: "Spaghetti",
          instructions: "Open jar of Spaghetti sauce.  Bring to simmer.  Boil water.  Cook pasta until done.  Combine pasta and sauce",
          ingredients: ["pasta", "8 cups water", "1 box spaghetti"],
          img: "pasta.jpg"
        },
        {
          id:1,
          title: "Milkshake",
          instructions: "Combine ice cream and milk.  Blend until creamy",
          ingredients: ["2 Scoops Ice cream", "8 ounces milk"],
          img: "milchShake.jpg"
        },
        {
          id:2,
          title: "Avocado Toast",
          instructions: "Toast bread.  Slice avocado and spread on bread.  Add salt, oil, and pepper to taste.",
          ingredients: ["2 slices of bread", "1 avocado", "1 tablespoon olive oil", "1 pinch of salt", "pepper"],
          img: "Toast.jpg"
        }, 
      ],
      nextRecipeId: 3,
      showForm: false
    }
    this.handelSave = this.handelSave.bind(this);
    this.onDelete = this.onDelete.bind(this)
  }
  handelSave(recipes){
    this.setState((prevState, props) =>{
      const newRecipe = {...recipes, id: this.state.nextRecipeId}
      return {
        nextRecipeId: prevState.nextRecipeId +1,
        recipes:[...this.state.recipes, newRecipe], 
        showForm: false
      }
    })
  }
  onDelete(id){
    const recipes = this.state.recipes.filter(recipes => recipes.id !== id)
    this.setState({recipes})
  }
  render() {
    const {showForm} = this.state;
    return (
     
      <div className="App">
       <Navbar onNewRecipe={()=> this.setState({showForm: true})}/>
        {showForm ?
           <RecipeInput 
            onSave={this.handelSave}
            onClose={() => this.setState({showForm: false})}
           />
        :null}
        <RecipeList onDelete={this.onDelete} recipes={this.state.recipes}/>
      </div>
    );
  }
}

export default RecipeApp;
