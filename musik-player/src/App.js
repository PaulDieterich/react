import React, { Component } from 'react';
import './App.css';
import Player from './components/Player';

class App extends Component {
  render() {
    return (
      <div className="App">
       <Player songs={songs}></Player>
          
      </div>
    );
  }
}

const songs = [
  { name:"Der Himmel Ist Blau", interpret:"Die Ärtzte", img:"https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Emblem_Die_%C3%84rzte2012.svg/220px-Emblem_Die_%C3%84rzte2012.svg.png"},
  { name:"Lass Sie Reden", interpret:"Die Ärtzte", img:"https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Emblem_Die_%C3%84rzte2012.svg/220px-Emblem_Die_%C3%84rzte2012.svg.png"},
  { name:"Junge", interpret:"Die Ärtzte", img:"https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Emblem_Die_%C3%84rzte2012.svg/220px-Emblem_Die_%C3%84rzte2012.svg.png"},
];

export default App;
