import React, { Component } from 'react';
import './Player.css'
class Player extends Component {
    constructor(props){
        super(props);
        this.state ={
            songID: 2,
            play: false
        }
        this.nextSong = this.nextSong.bind(this)
     
    }
    nextSong = () =>{
       
    }
    render() {
       
        const songs = this.props.songs.map((song, index)=>{
            if(index === this.state.songID){
                return <div> 
                            <span>  {song.name} </span> 
                            <span>  {song.interpret} </span> 
                        </div>
            }
        }) 
        return ( <div className="player">
                    <Img></Img>
                    <Name name={songs} ></Name>
                    <Play onNextSong={this.nextSong}></Play>
                    <Sound></Sound>
                </div>);
    }
}
 
export default Player;


class Img extends Component{
    render(){
        return( 
            <div className="img">
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Emblem_Die_%C3%84rzte2012.svg/220px-Emblem_Die_%C3%84rzte2012.svg.png" alt="img"></img>
            </div>
        )
    }
}
class Name extends Component{
    render() {
        return(
             <div className="name">
                <div> <span> {this.props.name}</span> <span>{this.props.interpret}</span></div>
            </div>
        )
    }
}
class Play extends Component{
    constructor(props){
        super(props);
        this.nextSong = this.nextSong.bind(this);
        this.prevSong = this.prevSong.bind(this);
        this.playSong = this.playSong.bind(this);
        this.state= {
            playOn : true
            }
        }
     

    playSong(){
       let playOn = this.state;
        if(playOn){
            playOn = false;
            console.log("Musik Pausiert  " + playOn )
            return  false;
        }
            playOn = true;
            console.log("Spielt  " + playOn )
            return playOn;
        
       
        
    }
    prevSong(e){
        e.preventDefault();
        console.log('Prev Song');
      
    }
        nextSong(e){
        e.preventDefault();
        console.log('Next Song');
      
    }
    render(){
        return(
            <div className="play">
                <div>
                    <span className="playButton" onClick={this.prevSong}> prev </span>
                    <span className="playButton"  onClick={this.playSong}>play </span>
                    <span className="playButton" onClick={this.nextSong}> next </span>
                </div>
            </div>
        )
    }
}
class Sound extends Component{
    render(){
        return( 
        <div className="sound">
               <input type="range" id="sound" min="0" max="100" step="1"/>
              
            
        </div>
        )
    }
}