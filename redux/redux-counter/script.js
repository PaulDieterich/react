// we need a reducer (rootReducer)
// We need some redux store and initalState
// We need some way to changing the state

const initalState = {
    count: 0
}

function rootReducer(state = initalState, action){
    let newState = Object.assign({}, state);
    switch(action.type){
        case "INCREMENT":
            newState.count++
            return  newState
        case "DECREMENT":
            newState.count--
            return  newState
        default: 
            return state

    }
}

const store = Redux.createStore(rootReducer);


$(document).ready(function(){
    $("#increment").on("click", function(){
        store.dispatch({
            type: "INCREMENT"
        });
        let currentState = store.getState()
        $("#counter").text(currentState.count);
        //dispatch some action to increment the count!
    });
    $("#decrement").on("click", function(){
        store.dispatch({
            type: "DECREMENT"
        });
        let currentState = store.getState()
        $("#counter").text(currentState.count);
        //dispatch some action to decrement the count!
    });
})